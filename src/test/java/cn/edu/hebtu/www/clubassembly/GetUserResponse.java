package cn.edu.hebtu.www.clubassembly;

import java.util.List;

/**
 * Created by root on 16-3-14.
 */
public class GetUserResponse {
    private User user;
    private List<Activity> activities;
    private String responseStr ;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<Activity> getActivities() {
        return activities;
    }

    public void setActivities(List<Activity> activities) {
        this.activities = activities;
    }

    public String getResponseStr() {
        return responseStr;
    }

    public void setResponseStr(String responseStr) {
        this.responseStr = responseStr;
    }

    /*    {
        "user": {
        "islogin": true,
                "name": "陈志兵",
                "mobile": "18233158677"
    },
        "activities": [
        {
            "pn": "健康理疗体验预约",
                "an": "",
                "aid": "A6F3D839-1AE7-9C2F-DE02-F6AD8600898D",
                "time_begin": "2016-02-21 20:00:00",
                "time_finish": "2016-02-28 17:00:00",
                "number_start": "1",
                "number_end": "50",
                "count": "50",
                "status": "1",
                "pic": "upload/19A8C354/BB1A/F1E5/B502/84EF574D6794/21456054247.png"
        }
        ]
    }*/


}
