package cn.edu.hebtu.www.clubassembly;

import cn.edu.hebtu.www.clubassembly.utils.BaseUtils;
import com.google.gson.reflect.TypeToken;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by root on 16-3-12.
 */
public class TestFecthNumber {

    private static CloseableHttpClient httpClient = HttpClientFactory.getHttpClientWithRetry();

    private static String baseUrl = "http://ovation.net.cn:8010/index.php";
    private static String loginUrl = baseUrl + "/login";
    private static String userurl = baseUrl + "/activity";
    private static String codeurl = baseUrl + "/code";

    @Test
    public void testxa() {
        String phoneNumber = "18233158677";
        String password = "czb123";
        String codeString = fetchNumber(phoneNumber, password);
        System.out.println(codeString);
    }

    public static String fetchNumber(String phoneNumber, String password) {
        CommonResponse loginResponse = getLoginStatusFromUrl(phoneNumber, password);
        if (loginResponse.getM().toString().equals("登录成功")) {
            System.out.println("login success,content:" + loginResponse.getResponseStr());
            GetUserResponse getUserResponse = getUserFromUrl();
            if (getUserResponse.getUser() != null && getUserResponse.getUser().islogin()) {
                System.out.println("get user success,content:" + getUserResponse.getResponseStr());
                boolean codeFlagFault = true;
                while (codeFlagFault) {
                    CommonResponse codeResponse = getCodesFromUrl();
                    if (codeResponse.getCodes() != null && codeResponse.getCodes().size() > 0) {
                        System.out.println("get code success,content:" + codeResponse.getResponseStr());
                        return codeResponse.getResponseStr();
                    } else {
                        System.out.println("get code fault,content:" + codeResponse.getResponseStr());
                        //目前不知道code格式,所以存在抢到号，但解析出错的情况
                        String codeString = codeResponse.getResponseStr().substring(codeResponse.getResponseStr().indexOf("codes"));
                        System.out.println(codeString);
                        //如果codes有非0的数字,那差不多就是抢到号了。
                        String codeNumber = hasDigit(codeString);
                        if (codeNumber != null && codeNumber.length() > 0 && !codeNumber.equals("0")) {
                            return codeResponse.getResponseStr();
                        }
                    }
                }
            } else {
                System.out.println("get user fault,content:" + getUserResponse.getResponseStr());
            }
        } else {
            System.out.println("login fault,content:" + loginResponse.getResponseStr());
        }
        return null;
    }

    public static CommonResponse getCodesFromUrl() {
        HttpGet httpGet = new HttpGet(codeurl);
        CloseableHttpResponse response = null;
        String codecontent = "";
        try {
            response = httpClient.execute(httpGet);
            if (response.getStatusLine().getStatusCode() == 200) {
                codecontent = EntityUtils.toString(response.getEntity());
                if (codecontent != null) {
                    codecontent = decodeUnicode(codecontent).trim();
                    codecontent = codecontent.replace("\uFEFF\uFEFF\uFEFF", "");
                    CommonResponse codeResponse = BaseUtils.gson.fromJson(codecontent, new TypeToken<CommonResponse>() {
                    }.getType());
                    codeResponse.setResponseStr(codecontent);
                    return codeResponse;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        CommonResponse codeResponse = new CommonResponse();
        codeResponse.setResponseStr(codecontent);
        return codeResponse;
    }

    public static CommonResponse getLoginStatusFromUrl(String phoneNumber, String password) {
        String strContent = "";
        try {
            String param = "?a=login&p%5Bmobile%5D=" + phoneNumber + "&p%5Bremember%5D=true&p%5Bpassword%5D=" + password;
            String baseUrl = loginUrl + param;
            HttpGet httpGet = new HttpGet(baseUrl);
            httpGet.setHeader("Accept", "text/plain");
            httpGet.setHeader("Accept-Encoding", "gzip, deflate, sdch");
            httpGet.setHeader("Accept-Language", "zh-CN,zh;q=0.8");
            httpGet.setHeader("Connection", "keep-alive");
            httpGet.setHeader("Host", "ovation.net.cn:8010");
            httpGet.setHeader("Referer", "http://ovation.net.cn:8010/index.html");
            httpGet.setHeader("User-Agent", "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.106 Safari/537.36");
            httpGet.setHeader("X-Requested-With", "XMLHttpRequest");
            CloseableHttpResponse response = httpClient.execute(httpGet);
            if (response.getStatusLine().getStatusCode() == 200) {
                strContent = EntityUtils.toString(response.getEntity());
                strContent = decodeUnicode(strContent).trim();
                strContent = strContent.replace("\uFEFF\uFEFF\uFEFF", "");
                CommonResponse loginResponse = BaseUtils.gson.fromJson(strContent, new TypeToken<CommonResponse>() {
                }.getType());
                loginResponse.setResponseStr(strContent);
                return loginResponse;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        CommonResponse loginResponse = new CommonResponse();
        loginResponse.setResponseStr(strContent);
        return loginResponse;
    }

    public static GetUserResponse getUserFromUrl() {
        HttpGet httpGet = new HttpGet(userurl);
        CloseableHttpResponse response = null;
        String usercontent = "";
        try {
            response = httpClient.execute(httpGet);
            if (response.getStatusLine().getStatusCode() == 200) {
                usercontent = EntityUtils.toString(response.getEntity());
                if (usercontent != null) {
                    usercontent = decodeUnicode(usercontent).trim();
                    usercontent = usercontent.replace("\uFEFF\uFEFF\uFEFF", "");
                    GetUserResponse getUserResponse = BaseUtils.gson.fromJson(usercontent, new TypeToken<GetUserResponse>() {
                    }.getType());
                    getUserResponse.setResponseStr(usercontent);
                    return getUserResponse;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        GetUserResponse getUserResponse = new GetUserResponse();
        getUserResponse.setResponseStr(usercontent);
        return getUserResponse;
    }

    public static String decodeUnicode(String theString) {

        char aChar;
        int len = theString.length();
        StringBuffer outBuffer = new StringBuffer(len);
        for (int x = 0; x < len; ) {
            aChar = theString.charAt(x++);
            if (aChar == '\\') {
                aChar = theString.charAt(x++);
                if (aChar == 'u') {
                    // Read the xxxx
                    int value = 0;
                    for (int i = 0; i < 4; i++) {
                        aChar = theString.charAt(x++);
                        switch (aChar) {

                            case '0':

                            case '1':

                            case '2':

                            case '3':

                            case '4':

                            case '5':

                            case '6':
                            case '7':
                            case '8':
                            case '9':
                                value = (value << 4) + aChar - '0';
                                break;
                            case 'a':
                            case 'b':
                            case 'c':
                            case 'd':
                            case 'e':
                            case 'f':
                                value = (value << 4) + 10 + aChar - 'a';
                                break;
                            case 'A':
                            case 'B':
                            case 'C':
                            case 'D':
                            case 'E':
                            case 'F':
                                value = (value << 4) + 10 + aChar - 'A';
                                break;
                            default:
                                throw new IllegalArgumentException(
                                        "Malformed   \\uxxxx   encoding.");
                        }

                    }
                    outBuffer.append((char) value);
                } else {
                    if (aChar == 't')
                        aChar = '\t';
                    else if (aChar == 'r')
                        aChar = '\r';

                    else if (aChar == 'n')

                        aChar = '\n';

                    else if (aChar == 'f')

                        aChar = '\f';

                    outBuffer.append(aChar);

                }

            } else

                outBuffer.append(aChar);

        }

        return outBuffer.toString();

    }

    public static String hasDigit(String content) {
        Pattern p = Pattern.compile("[^0-9]");
        Matcher m = p.matcher(content);
        String all = m.replaceAll("");
        return all;
    }

}
